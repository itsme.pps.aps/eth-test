var config = require('../config');
var Web3 = require('web3');
var path = require('path');
var Tx = require('ethereumjs-tx').Transaction;
const hdkey = require("ethereumjs-wallet/hdkey")
const bip39 = require("bip39")

var web3 = new Web3();
var web3RpcAddr = config.geth.httpEndPoint;
const {STATUS} = require('../config')
const l = require('../logger').root.child({ 'module': 'web3 service' });


var fileName = path.basename(__filename);

web3.setProvider(new web3.providers.HttpProvider(web3RpcAddr));

exports.createAddress = (password) => {
  try{
    var data = web3.eth.accounts.create(password);
    var keystore = web3.eth.accounts.encrypt(data.privateKey, password)
    var res = {
      'address': data.address,
      'privateKey' : data.privateKey,
      'keystore' : keystore
    }
    return res;
  }
  catch(e){
    l.error(fileName, '| craete address | error', e);
    return null;
  }
}

exports.generateMnemonic = async() => {
  return bip39.generateMnemonic()
}

exports.generateAddressFromMnemonic = async(mnemonic, index=0) => {
  const seed = await bip39.mnemonicToSeed(mnemonic)
  const rootKey = hdkey.fromMasterSeed(seed)
  const hardenedKey = rootKey.derivePath("m/44'/60'/0'/0")
  const childKey = hardenedKey.deriveChild(index)
  const wallet = childKey.getWallet()
  const address = wallet.getAddress()
  const privateKey = wallet.getPrivateKey()
  return {
    address: address.toString("hex"),
    privateKey : privateKey.toString("hex"),
    mnemonic
  }
}

exports.getWeiBalance = async (address) => {
  var logmsg = fileName + ' getBalance |'
    return web3.eth.getBalance(address)
    .then(bal => {
      return bal;
    })
    .catch(err => {
      l.error(logmsg, 'something wrong happened', err)
      return 0;
    })
}


exports.checkTxStatus = async(hash) => {
  var logmsg = fileName + ' check Tx status |'
  try{
    var txReceipt = await web3.eth.getTransactionReceipt(hash);
    if(txReceipt){
      if(txReceipt.status){ // txReceipt.status == '0x1'
        return STATUS.COMPLETED
      }else{
        return STATUS.FAILED
      }
    }
    else{
      return STATUS.PENDING
    }
  }
  catch(e){
    l.error(logmsg, 'error =>', e)
    return STATUS.PENDING
  }
}

exports.signTransaction = async (payload) =>{
  let logmsg = fileName + ' | signTransaction |'
  try{
    let { from, to, value, privKey } = payload

    let [nonce, gasPrice] = await Promise.all([
        this.getNonce(from),
        this.getGasPrice()
    ])
    let gas = 21000

    let txParams = {
      to,
      value: web3.utils.toHex(value),
      gasPrice: web3.utils.toHex(gasPrice),
      gas: web3.utils.toHex(gas),
      nonce: web3.utils.toHex(nonce),
      //chain:'ropsten'
    }

    var tx = new Tx(txParams, {'chain':'ropsten'})
    privKey = await _validatePrivKey(privKey)
    privKey = new Buffer(privKey, 'hex')
    tx.sign(privKey)
    let serializedTx = "0x" + tx.serialize().toString('hex')
    return serializedTx
  } catch(e){
    l.error(logmsg, 'Unable to sign tx', e.message)
    return null
  }
}

exports.submitSignedTx = async(serializedTx) => {
  let logmsg = fileName + ' | submitSignedTx |'
  try{
    return new Promise((fullfill, reject) => {
      web3.eth.sendSignedTransaction(serializedTx)
        .on('transactionHash', txHash => {
          l.info(logmsg, 'transaction sent. hash =>', txHash)
          return fullfill({success: true, txHash : txHash})
        })
        .on('error', e => {
          l.error(logmsg, 'unable to send tx', e);
          return fullfill({success: false, message: e})
        })
    })
  } catch(e){
    l.error(logmsg, 'Unable to submit tx',e.message);
    return {success: false, message: e.message };
  }
}

exports.getGasPrice = async() => {
  let logmsg = fileName + ' | getGasPrice |'
  try{
    return await web3.eth.getGasPrice();
  } catch(e){
    l.error(logmsg, 'Unable to fetch gas price', e.message)
    return null;
  }
}

exports.getNonce = async(from) => {
  try{
    return await web3.eth.getTransactionCount(from, 'pending')
  } catch(e){
    return null;
  }
}

exports.compare = (a,b) => {
    return web3.utils.toBN(a).lte(web3.utils.toBN(b))
}

async function _validatePrivKey(privKey){
  if(privKey.startsWith('0x')) privKey = privKey.substr(2)
  return privKey
}
