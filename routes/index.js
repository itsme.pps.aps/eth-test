const router = require('express').Router()

const ethService = require('../services/web3.service')

router.get('/balance/:address', async(req,res) => {
    try{
        let {address} = req.params
        if(!address) {
            return res.status(422).json({ success: false, message: 'No address is provided'})
        }
        let balance = await ethService.getWeiBalance(address)
        return res.status(200).json({success: true, weiBalance: balance})
    } catch(e) {
        return res.status(500).json({success: false, message: e.message || 'Something went wrong'})
    }
})

router.post('/create-address', async(req, res) => {
    try {
        let { mnemonic, index } = req.body
        if(!mnemonic || !index) {
            return res.status(400).json({ success: false, message: 'Mnemonic and index are required'})
        }
        let addressDetails = await ethService.generateAddressFromMnemonic(mnemonic, index)
        return res.status(201).json({ success: true, addressDetails})
    } catch(e) {
        return res.status(500).json({success: false, message: e.message || 'Something went wrong'})
    }
})

router.get('/tx-status/:txHash', async(req, res) => {
    try {
        let {txHash} = req.params
        if(!txHash) return res.status(422).json({ success: false, message: 'Invalid tx hash'})
        let status = await ethService.checkTxStatus(txHash)
        return res.status(200).json({success: true, status: status})
    } catch(e) {
        return res.status(500).json({success: false, message: e.message || 'Something went wrong'})
    }
})

router.post('/send-eth', async(req,res, next) => {
    try {
        let { from, to, weiAmount, privKey } = req.body
        if(!from || !to || !weiAmount || !privKey) {
            return res.status(422).json({ success: false, message: 'Missing params'})
        }
        let weiBalance = await ethService.getWeiBalance(from)
        console.log({weiBalance, weiAmount})
        // balance check -- this should be amount+estimated Fee in real
        if(ethService.compare(weiBalance, weiAmount)) {
            return res.status(400).json({ success: false, message: 'Insufficient balance0'})
        }

        let signedTx = await ethService.signTransaction({
            from, to, value: weiAmount, privKey
        })

        let data = await ethService.submitSignedTx(signedTx)
        return res.status(200).json(data)

    } catch(e) {
        return res.status(500).json({success: false, message: e.message || 'Something went wrong'})
    }
})

router.get('/generate-mnemonic', async(req, res) => {
    try {
        let mnemonic =  await ethService.generateMnemonic()
        return res.status(200).json({ success: true, mnemonic })
    } catch(e) {
        return res.status(500).json({success: false, message: e.message || 'Something went wrong'})
    }
})


module.exports = router
