const express = require('express')
const helmet = require('helmet')
const bodyParser = require('body-parser')

const config = require('./config')

const routes = require('./routes')

const app = express()

app.use(helmet())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))

app.use('/api/v1',routes)

app.listen(config.webPort, ()=>{
    console.log(`listening on port ${config.webPort}`)
})

