module.exports = {
    webPort: 3010,
    geth: {
        httpEndPoint: ''
    },
    STATUS: {
        COMPLETED: 'COMPLETED',
        FAILED: 'FAILED',
        PENDING: 'PENDING'
    }
}